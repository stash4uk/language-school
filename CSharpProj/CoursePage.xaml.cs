﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSharpProj
{
    /// <summary>
    /// Interaction logic for CoursePage.xaml
    /// </summary>
    public partial class CoursePage : Page
    {
        Courses course;
        public CoursePage()
        {
            InitializeComponent();
            FetchCourseRecords();
        }

        public void FetchCourseRecords()
        {
            lvCourses.ItemsSource = Globals.db.Courses.ToList().OrderBy(c => c.Description).ThenBy(c => c.StartDate);
            lvCourses.Items.Refresh();
        }

        public void RefreshCourseData()
        {
            tbCourse.Text = string.Empty;
            dpStartDate.SelectedDate = DateTime.Today;
            dpEndDate.SelectedDate = DateTime.Today;
            cbMonday.IsChecked = false;
            cbTuesday.IsChecked = false;
            cbWednesday.IsChecked = false;
            cbThursday.IsChecked = false;
            cbFriday.IsChecked = false;
        }

        private void lvCourses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvCourses.SelectedItem == null) { return; }
            course = (Courses)lvCourses.SelectedItem;

            tbCourse.Text = course.Description;
            dpStartDate.SelectedDate = course.StartDate;
            dpEndDate.SelectedDate = course.EndDate;
            cbMonday.IsChecked = course.Monday;
            cbTuesday.IsChecked = course.Tuesday;
            cbWednesday.IsChecked = course.Wednesday;
            cbThursday.IsChecked = course.Thursday;
            cbFriday.IsChecked = course.Friday;
            btnAddCourse.IsEnabled = false;
            btnUpdateCourse.IsEnabled = true;
            btnDeleteCourse.IsEnabled = true;
        }

        private void btnAddCourse_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbCourse.Text))
            {
                MessageBox.Show("Please input a course", "Course missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else if (dpStartDate.SelectedDate == null || dpEndDate.SelectedDate == null)
            {
                MessageBox.Show("Please select dates", "Missing dates", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else if (DateTime.Compare((DateTime)dpStartDate.SelectedDate, (DateTime)dpEndDate.SelectedDate) > 0)
            {
                MessageBox.Show("Invalid date: End Date must be later than Start Date or they must be the same)", "Wrong dates", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else if (IsDaysChecked())
            {
                MessageBox.Show("Please select at least one day", "Days missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else
            {
                Courses newCourse = new Courses
                {
                    Description = (String)tbCourse.Text,
                    StartDate = (DateTime)dpStartDate.SelectedDate,
                    EndDate = (DateTime)dpEndDate.SelectedDate,
                    Monday = (Boolean)cbMonday.IsChecked,
                    Tuesday = (Boolean)cbTuesday.IsChecked,
                    Wednesday = (Boolean)cbWednesday.IsChecked,
                    Thursday = (Boolean)cbThursday.IsChecked,
                    Friday = (Boolean)cbFriday.IsChecked,
                };


                Globals.db.Courses.Add(newCourse);
                Globals.db.SaveChanges();
                FetchCourseRecords();
                RefreshCourseData();
            }
        }

        private void btnUpdateCourse_Click(object sender, RoutedEventArgs e)
        {
            if (IsDaysChecked())
            {
                MessageBox.Show("Must be check at least one day", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (DateTime.Compare((DateTime)dpStartDate.SelectedDate, (DateTime)dpEndDate.SelectedDate) < 0)
            {
                course.Description = (String)tbCourse.Text;
                course.StartDate = (DateTime)dpStartDate.SelectedDate;
                course.EndDate = (DateTime)dpEndDate.SelectedDate;
                course.Monday = (Boolean)cbMonday.IsChecked;
                course.Tuesday = (Boolean)cbTuesday.IsChecked;
                course.Wednesday = (Boolean)cbWednesday.IsChecked;
                course.Thursday = (Boolean)cbThursday.IsChecked;
                course.Friday = (Boolean)cbFriday.IsChecked;
                Globals.db.SaveChanges();
                FetchCourseRecords();
                RefreshCourseData();
            }
            else
            {
                MessageBox.Show("Invalid Date(End Date must be later Start Date or they must not be same date)", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void btnDeleteCourse_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Are you sure to delete?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                return;
            }
            Globals.db.Courses.Remove(course);
            Globals.db.SaveChanges();
            FetchCourseRecords();
            RefreshCourseData();
        }

        private Boolean IsDaysChecked()
        {
            return (!((Boolean)cbMonday.IsChecked || (Boolean)cbTuesday.IsChecked || (Boolean)cbWednesday.IsChecked || (Boolean)cbThursday.IsChecked || (Boolean)cbFriday.IsChecked));
        }
    }
}

