﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSharpProj
{
    /// <summary>
    /// Interaction logic for StudentPage.xaml
    /// </summary>
    public partial class StudentPage : Page
    {
        Students student;
        public StudentPage()
        {
            InitializeComponent();
            FetchStudentRecords();
        }

        public void FetchStudentRecords()
        {
            lvStudents.ItemsSource = Globals.db.Students.ToList().OrderBy(s => s.Name);
            lvStudents.Items.Refresh();
        }

        private void lvStudents_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvStudents.SelectedItem == null) { return; }
            student = (Students)lvStudents.SelectedItem;
            tbStudentName.Text = student.Name;
            btnAddStudent.IsEnabled = false;
            btnUpdateStudent.IsEnabled = true;
            btnDeleteStudent.IsEnabled = true;
        }

        private void btnAddStudent_Click(object sender, RoutedEventArgs e)
        {
            Students newStudent = new Students
            {
                Name = tbStudentName.Text,
            };
            if (string.IsNullOrEmpty(newStudent.Name))
            {
                MessageBox.Show("Please input a name", "Name missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            Globals.db.Students.Add(newStudent);
            Globals.db.SaveChanges();
            FetchStudentRecords();
            tbStudentName.Text = string.Empty;
        }

        private void btnUpdateStudent_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbStudentName.Text))
            {
                MessageBox.Show("Please input a name", "Name missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            student.Name = tbStudentName.Text;
            Globals.db.SaveChanges();
            FetchStudentRecords();
            tbStudentName.Text = string.Empty;
        }

        private void btnDeleteStudent_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Are you sure to delete?", "Confirm", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.No)
            {
                return;
            }
            Globals.db.Students.Remove(student);
            Globals.db.SaveChanges();
            FetchStudentRecords();
            tbStudentName.Text = string.Empty;
        }

        private void btnRegistrations_Click(object sender, RoutedEventArgs e)
        {
            var win = new WizardWindow();
            List<Students> students = new List<Students>();
            students = Globals.db.Students.ToList();
            foreach (Students student in students)
            {
                win.lvWizardStudent1.Items.Add(student);
            }
            List<Courses> courses = new List<Courses>();
            courses = Globals.db.Courses.ToList();
            win.comboCourse.ItemsSource = courses.OrderBy(c => c.Description).ToList();
            
            List<Teachers> teachers = new List<Teachers>();
            teachers = Globals.db.Teachers.ToList();
            win.comboTeachers.ItemsSource = teachers.OrderBy(t => t.Name).ToList();
            
            win.ShowDialog();
        }
    }
}
