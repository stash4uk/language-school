﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpProj
{
    public class NameCourse
    {
        public string Name { get; set; }
        public string Course { get; set; }
        public string Teacher { get; set; }
    }
    public partial class Courses
    {
        public string Days
        {
            get
            {
                return GenerateDays();
            }
        }

        public string GenerateDays()
        {
            string Days = "";
            if (Monday == true)
            {
                Days += "Monday";
            }

            if (Tuesday == true)
            {
                if (Days.Equals(""))
                {
                    Days += "Tuesday";
                }
                else
                {
                    Days += "\nTuesday";
                }
            }

            if (Wednesday == true)
            {
                if (Days.Equals(""))
                {
                    Days += "Wednesday";
                }
                else
                {
                    Days += "\nWednesday";
                }
            }

            if (Thursday == true)
            {
                if (Days.Equals(""))
                {
                    Days += "Thursday";
                }
                else
                {
                    Days += "\nThursday";
                }
            }

            if (Friday == true)
            {
                if (Days.Equals(""))
                {
                    Days += "Friday";
                }
                else
                {
                    Days += "\nFriday";
                }
            }
            return Days;
        }
    }

    public class Registrations
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string CourseDescription { get; set; }
        public int CourseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Days { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
    }
}
    



