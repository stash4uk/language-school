﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSharpProj
{
    /// <summary>
    /// Interaction logic for RegistrationPage.xaml
    /// </summary>
    public partial class RegistrationPage : Page
    {
        List<Registrations> reg = new List<Registrations>();
        public RegistrationPage()
        {
            InitializeComponent();
            FetchRegistrationRecords();
        }
        public void FetchRegistrationRecords()
        {
            reg.Clear();
            Globals.db.Courses.ToList().ForEach(c => 
            {
                c.Students.ToList().ForEach(s => 
                {
                    c.Teachers.ToList().ForEach(t =>
                    {
                        reg.Add(new Registrations
                        {
                            StudentName = s.Name,
                            CourseDescription = c.Description,
                            StartDate = c.StartDate,
                            EndDate = c.EndDate,
                            Days = c.Days,
                            TeacherName = t.Name,
                            StudentId = s.StudentId,
                            TeacherId = t.TeacherId,
                            CourseId = c.CourseId
                        });
                   
                    });
                });
                
            });
            lvRegistrations.ItemsSource = reg;
            lvRegistrations.Items.Refresh();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Registrations regSelected = (Registrations)lvRegistrations.SelectedItem;
            reg.Remove(regSelected);
            Courses course = Globals.db.Courses.ToList().Where(c => c.CourseId == regSelected.CourseId).FirstOrDefault();
            course.Students.Remove(course.Students.Where(s => s.StudentId == regSelected.StudentId).FirstOrDefault());
            //course.Teachers.Remove(course.Teachers.Where(t => t.TeacherId == regSelected.TeacherId).FirstOrDefault());

            Globals.db.SaveChanges();
            FetchRegistrationRecords();
        }

        private void lvRegistrations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
