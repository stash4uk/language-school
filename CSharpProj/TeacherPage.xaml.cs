﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace CSharpProj
{
    /// <summary>
    /// Interaction logic for TeacherPage.xaml
    /// </summary>
    public partial class TeacherPage : Page
    {
        Teachers teacher;
        public TeacherPage()
        {
            InitializeComponent();
            FetchTeacherRecords();
        }

        public void FetchTeacherRecords()
        {
            {
                lvTeachers.ItemsSource = Globals.db.Teachers.ToList().OrderBy(t => t.Name);
                lvTeachers.Items.Refresh();
            }
        }

        private void lvTeachers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvTeachers.SelectedItem == null) { return; }
            teacher = (Teachers)lvTeachers.SelectedItem;
            tbTeacherName.Text = teacher.Name;
            btnAddTeacher.IsEnabled = false;
            btnUpdateTeacher.IsEnabled = true;
            btnDeleteTeacher.IsEnabled = true;
        }

        private void btnAddTeacher_Click(object sender, RoutedEventArgs e)
        {
            Teachers newTeacher = new Teachers
            {
                Name = tbTeacherName.Text,
            };

            if (string.IsNullOrEmpty(newTeacher.Name))
            {
                MessageBox.Show("Please input a name", "Name missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            Globals.db.Teachers.Add(newTeacher);
            Globals.db.SaveChanges();
            FetchTeacherRecords();
            tbTeacherName.Text = string.Empty;
        }

        private void btnUpdateTeacher_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbTeacherName.Text))
            {
                MessageBox.Show("Please input a name", "Name missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            teacher.Name = tbTeacherName.Text;
            Globals.db.SaveChanges();
            FetchTeacherRecords();
            tbTeacherName.Text = string.Empty;
        }

        private void btnDeleteTeacher_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Are you sure to delete?", "Confirm", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.No)
            {
                return;
            }
            Globals.db.Teachers.Remove(teacher);
            Globals.db.SaveChanges();
            FetchTeacherRecords();
            tbTeacherName.Text = string.Empty;
        }
    }
}
