﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSharpProj
{
    /// <summary>
    /// Interaction logic for WizardWindow.xaml
    /// </summary>
    public partial class WizardWindow : Window
    {
        public WizardWindow()
        {
            InitializeComponent();
        }

        private void lvWizardStudent1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvWizardStudent1.SelectedIndex == -1)
            {
                return;
            }
            btnWizardAdd.IsEnabled = true;
            btnWizardRemove.IsEnabled = false;
        }

        private void lvWizardStudent2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvWizardStudent2.SelectedIndex == -1)
            {
                return;
            }
            btnWizardAdd.IsEnabled = false;
            btnWizardRemove.IsEnabled = true;
        }

        private void lvWizardStudent1Add_Click(object sender, RoutedEventArgs e)
        {
            if (lvWizardStudent1.SelectedItem == null)
            {
                MessageBox.Show("Please select students", "Student missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            ArrayList list = new ArrayList(lvWizardStudent1.SelectedItems);
            foreach (Students item in list)
            {
                lvWizardStudent2.Items.Add(item);
                lvWizardStudent1.Items.Remove(item);
            }
        }

        private void lvWizardStudent2Remove_Click(object sender, RoutedEventArgs e)
        {
            ArrayList list = new ArrayList(lvWizardStudent2.SelectedItems);
            foreach (Students item in list)
            {
                lvWizardStudent2.Items.Remove(item);
                lvWizardStudent1.Items.Add(item);
            }
        }

        private void btnNextWizard(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            if (Wizard.CurrentPage == Page1 && comboTeachers.SelectedItem == null)
            {
                MessageBox.Show("Please select a Teacher", "Teacher missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                e.Cancel = true;
            }

            if (Wizard.CurrentPage == Page1 && comboCourse.SelectedItem == null)
            {
                MessageBox.Show("Please select a Course", "Course missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                e.Cancel = true;
            }
            
            if (Wizard.CurrentPage == Page2)
            {
                if (lvWizardStudent2.Items.Count == 0)
                {
                    MessageBox.Show("Please select a Student", "Student missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                    e.Cancel = true;
                }
                else
                {
                    lvWizardStudent3.Items.Clear();
                    foreach (Students student in lvWizardStudent2.Items)
                    {
                        var course = (Courses)comboCourse.SelectedItem;
                        string valueCourse = course.Description;
                        var teacher = (Teachers)comboTeachers.SelectedItem;
                        string valueTeacher = teacher.Name;
                        course.Students.Add(student);
                        course.Teachers.Add(teacher);

                        lvWizardStudent3.Items.Add(new NameCourse { Name = student.Name, Course = valueCourse, Teacher = valueTeacher });
                    }
                    var course2 = (Courses)comboCourse.SelectedItem;
                    var teacher2 = (Teachers)comboTeachers.SelectedItem;
                    course2.Teachers.Add(teacher2);
                }
            }
        }

        private void btnFinishWizard(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            Globals.db.SaveChanges();
            Close();
        }
    }
}
