﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSharpProj
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Globals.db = new LanguageSchoolDbConnection();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tabTeachers.IsSelected)
            {
                Main.Content = new TeacherPage();
            }
            else if (tabStudents.IsSelected)
            {
                Main.Content = new StudentPage();
            }
            else if (tabCourses.IsSelected)
            {
                Main.Content = new CoursePage();
            }
            else if (tabRegistrations.IsSelected)
            {
                Main.Content = new RegistrationPage();
            }
        }
    }
}


        